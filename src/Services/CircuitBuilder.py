from braket.circuits import Circuit
from qiskit import QuantumCircuit

class CircuitBuilder():
    """
    Base class for circut builder.
    @todo: Migrate to Factory as well.
    """

    def get_full_circut(self, n_qubits, tokens, alpha, beta):
        pass

class CircuitBuilderAws(CircuitBuilder):
    """
    Create AWC circut instance.
    """

    def generate_problem_operator_gates(self, n_qubits, tokens, alpha):
        """
        Generate problem Hamiltonian gates.
        """
        circ = Circuit()
        for token in tokens:
            theta = 2 * token['coeff'] * alpha
            if (len(token['indices']) == 1):
                rz = Circuit().rz(token['indices'][0], theta)
                circ.add(rz)
            else:
                last_index = len(token['indices']) - 1
                for i_index, value in enumerate(token['indices']):
                    if (i_index < last_index):
                        j_index = i_index + 1
                        if (i_index == 0):
                            cx = Circuit().h(token['indices'][j_index]).cz(token['indices'][i_index], token['indices'][j_index]).h(token['indices'][j_index])
                            circ.add(cx)
                        rz = Circuit().rz(token['indices'][i_index], theta)
                        circ.add(rz)
                    elif (i_index == last_index):
                        cx = Circuit().h(token['indices'][i_index]).cz(token['indices'][i_index - 1], token['indices'][i_index]).h(token['indices'][i_index])
                        circ.add(cx)
        return circ

    def generate_driver_operator_gates(self, n_qubits, beta):
        """
        Returns circuit for driver Hamiltonian U(Hb, beta)
        """
        # instantiate circuit object
        circ = Circuit()
        for node in range(0, n_qubits):
            rx_on_all = Circuit().rx(node, -.5 * beta)
            circ.add(rx_on_all)
    
        return circ

    def get_full_circut(self, n_qubits, tokens, alpha, beta):
        """
        Build the whole circuit instance.
        """
        circ = Circuit()
        X_on_all = Circuit().x(range(0, n_qubits))
        circ.add(X_on_all)
        H_on_all = Circuit().h(range(0, n_qubits))
        circ.add(H_on_all)
        
        circ.add(self.generate_problem_operator_gates(n_qubits, tokens, alpha))
        circ.add(self.generate_driver_operator_gates(n_qubits, beta))
        return circ

class CircuitBuilderIbm(CircuitBuilder):
    """
    IBMq circuit builder.
    """

    def generate_problem_operator_gates(self, n_qubits, tokens, alpha):
        """
        Problem Hamiltonian gates constructor.
        """
        qc = QuantumCircuit(n_qubits, n_qubits)
        for token in tokens:
            theta = 2 * token['coeff'] * alpha
            if (len(token['indices']) == 1):
                qc.rz(theta, token['indices'][0])
            else:
                last_index = len(token['indices']) - 1
                for i_index, value in enumerate(token['indices']):
                    if (i_index < last_index):
                        j_index = i_index + 1
                        if (i_index == 0):
                            qc.cx(token['indices'][i_index], token['indices'][j_index])
                        qc.rz(theta, token['indices'][i_index])
                    elif (i_index == last_index):
                        qc.cx(token['indices'][i_index - 1], token['indices'][i_index])
        return qc

    def generate_driver_operator_gates(self, n_qubits, beta):
        """
        Driver Hamiltonian gates constructor.
        """
        qc = QuantumCircuit(n_qubits, n_qubits)
        for node in range(n_qubits):
            qc.rx(-.5 * beta, node)
            
        return qc

    def get_full_circut(self, n_qubits, tokens, alpha, beta):
        """
        Build the whole circuit instance.
        """
        circ = QuantumCircuit(n_qubits, n_qubits)
        print("here I am")
        circ.h(range(n_qubits))
        circ += self.generate_problem_operator_gates(n_qubits, tokens, alpha)
        circ += self.generate_driver_operator_gates(n_qubits, beta)
        
        circ.barrier(range(n_qubits))
        circ.measure(range(n_qubits), range(n_qubits))
        return circ


def create_circuit(type, n_qubits, tokens, alpha, beta):
    print(type)
    if (type == 'AWS'):
        print("Creating the AWS circuit.")
        return CircuitBuilderAws().get_full_circut(n_qubits, tokens, alpha, beta)
    elif (type == "IBM"):
        print("Creating the IBM-Q circuit. I am here")
        return CircuitBuilderIbm().get_full_circut(n_qubits, tokens, alpha, beta)