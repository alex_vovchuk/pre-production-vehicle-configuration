# Imports.
import re

class ExpressionParser():
    """
    Class allowing to tokenize the symbolic expression.
    It takes sympy expanded circut formula and returns the tokens
    that are required to create the circut.
    """

    def pre(self, expr, n, expressions):
        """
        Recursively walk through the expression tree,
        reaching the necessary level of the expression.
        """
        if (n == 1):
            expressions.append(str(expr))
        n += 1
        if (n < 2):
            for arg in expr.args:
                self.pre(arg, n, expressions)
                
    def get_expressions_list(self, h, n):
        """
        Get the expressions list of terms from the Hamiltonian.
        """
        expressions = []
        self.pre(h.expand(), n, expressions)
        return expressions

    def parse_keys(self, expression_term):
        """
        Parse the keys of each term to split the term = alpha * Z[j] * Z[k] * Z[n].
        Get the item containing the alpha coefficient, and list of indices.
        """
        term = {}
        if (len(expression_term) >= 2):
            term['coeff'] = float(expression_term[0])
            term['indices'] = []
            for index, literal in enumerate(expression_term):
                if (index > 0):
                    print(expression_term)
                    res = re.search("\d+", literal)
                    if (res != None):
                        term['indices'].append(int(res[0]))
        else:
            return False
        return term
                    
    def parse_expressions(self, expressions):
        """
        Perform the parse of the Hamiltonian expression.
        """
        terms = []
        for exp in expressions:
            # We are not interested in the terms without Z
            if ("Z" in exp):
                # Filter out powers of I
                exp = re.sub(r"\*I\*\*[0-9]+", "", exp)
                # Filter out plain I
                exp = re.sub(r"\*I", "", exp)
                matching = re.findall(r"Z\[\d+\]\*\*[0-9]+", exp)
                # strip all powers of Z.
                exp = re.sub(r"\*Z\[\d+\]\*\*[0-9]+", "", exp)

                """
                Go through all the matches to convert z**i to z*z*z ... *z i times.
                Then check if power is ODD as for Pauli matrices we have multiplication rules.
                """ 
                # @todo: split this function.
                if matching != None:
                    add_string = ''
                    for match_item in matching:
                        split = match_item.split('**')
                        if (int(split[1]) % 2 != 0):
                            add_string += '*' + split[0]
                exp += add_string
                # Unpack expression to list
                exp_list = exp.split("*")

                # Parse keys and indicies of Pauli Matrices.
                term = self.parse_keys(exp_list)
                if ((term != None) and (term != False)):
                    terms.append(term)
        return terms

    def parse(self, expression):
        """
        Perform parsing action.
        """
        expressions = self.get_expressions_list(expression, 0)
        return self.parse_expressions(expressions)


def parse_hamiltonian(expression):
    """
    Performes parsing of the expression.
    """
    return ExpressionParser().parse(expression)