#import
from braket.devices import LocalSimulator
from qiskit import Aer, execute

class BackEnd():
    def __init__(self, type):
        self.type = type
        self.create()

    def create(self):
        pass;

    def run(self, circuit, shots):
        pass;

class AwsBackEnd(BackEnd):

    def create(self):
        """
        Create appropriate backend. Here will be factory.
        """
        if (self.type == 'local'):
            self.backend = LocalSimulator()
        else:
            print("This should be handled in the error!")

    def run(self, circuit, shots):
        """
        Perform the task.
        """
        print("Running the AWS local simulation")
        task = self.backend.run(circuit, shots=shots)
        return task.result()

class IbmBackEnd(BackEnd):

    def create(self):
        """
        Create appropriate backend. Here will be factory.
        """
        if (self.type == 'local'):
            self.backend = Aer.get_backend('qasm_simulator')
        else:
            print("This should be handled in the error!")

    def run(self, circuit, shots):
        """
        Perform the task.
        """
        print("Running the IBM local simulation")
        frequencies = execute(circuit, self.backend, shots=shots, seed_simulator=7).result().get_counts()
        return frequencies

def run_circuit(backend, type, circuit, shots):
    if (backend == 'AWS'):
        aws = AwsBackEnd(type)
        return aws.run(circuit, shots)
    elif (backend == 'IBM'):
        aws = IbmBackEnd(type)
        return aws.run(circuit, shots)
