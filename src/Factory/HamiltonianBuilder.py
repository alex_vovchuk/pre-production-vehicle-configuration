import sympy as sym
from sympy import symbols, IndexedBase, Idx
from sympy import Sum, KroneckerDelta, Rational, Pow, Mul

class Hamiltonian():
    """
    Class representing the given problem Hamiltonian.
    """

    def build(self, constraints):
        pass

    """
    Build the single OR clause.
    """
    def get_or_expression(self, expression):
        # Identity matrix.
        I = symbols('I')
        Z = IndexedBase("Z")
        # Coefficient.
        m = 0.5
        # Hamiltonian dictionary.
        H_dict = {}
        for index, clause in enumerate(expression):
            # Add dictionary of clauses.
            H_dict[index] = clause["weight"] * m * (I + clause["sign"] * Z[clause["key"]])

        res = None
        for index, term in H_dict.items():
            if (index == 0):
                res = term
            else:
                res *= term
        return res

    def get_clause_expression(self, clause):
        indices = []
        for index in clause['items']:
            key = abs(index)
            sign = 1 if index > 0 else -1
            if index == 0:
                sign = 1
            item = {
                "key": symbols(str(key - 1)),
                "sign": sign,
                "weight": clause['weight']
            }
            indices.append(item)
        return indices

class nSatHamiltonian(Hamiltonian):
    """
    Class building the N-SAT Hamiltonian from a given problem.
    """
    def build(self, constraints):
        print("App: Launched with the N-Sat Hamiltonian Creator.")
        # Pauli matrix base.
        Hamiltonians = {}
        for index, clause in enumerate(constraints):
            expression = self.get_clause_expression(clause)
            Hamiltonians[index] = self.get_or_expression(expression)
        result = None
        for index, term in Hamiltonians.items():
            if index == 0:
                result = term
            else:
                result *= term
        return result

class maxSatHamiltonian(Hamiltonian):
    """
    Class building the weighted max-N-SAT Hamiltonian from a given problem.
    """
    def build(self, constraints):
        Hamiltonians = {}
        for index, clause in enumerate(constraints):
            expression = self.get_clause_expression(clause)
            Hamiltonians[index] = self.get_or_expression(expression)
        result = None
        for index, term in Hamiltonians.items():
            if index == 0:
                result = term
            else:
                result += term
        return result

class HamiltonianCreator():
    """
    Class allowing to construct the Hamiltonian for given problem.
    Factory Creator class.
    """

    def construct_hamiltonian(self):
        """
        Currently it doesn't contain any default realization of fabric method.
        """
        pass

    def build_hamiltonian(self, constraints):
        """
        Call the construct hamiltonian factory method to fetch exact hamiltonian.
        """

        hamiltonian = self.construct_hamiltonian()

        result = hamiltonian.build(constraints)
        return result

class nSatHamiltonianCreator(HamiltonianCreator):
    """
    Creator allowing to build the N-SAT problem Hamiltonian
    """
    def construct_hamiltonian(self) -> Hamiltonian:
        return nSatHamiltonian()

class maxSatHamiltonianCreator(HamiltonianCreator):
    """
    Creator allowing to build the N-SAT problem Hamiltonian
    """
    def construct_hamiltonian(self) -> Hamiltonian:
        return maxSatHamiltonian()

"""
Function allowing construction of any kind of Hamiltonian by key.

@param type
  Type  of selected hamiltonian.
@param constraints
  Constraints of the  selected problem.
"""
def construct_hamiltonian(type, constraints):
    if (type == 'n-sat'):
        return nSatHamiltonianCreator().build_hamiltonian(constraints)
    if (type == 'max-sat'):
        return maxSatHamiltonianCreator().build_hamiltonian(constraints)
    else:
        return False

        